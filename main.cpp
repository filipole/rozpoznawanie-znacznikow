#include "opencv2/opencv.hpp"
#include <opencv2/aruco.hpp>
#include <vector>
#include <opencv2/core.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>
#include <iostream>
#include <stdio.h>



cv::Mat detectMarkers(cv::Mat frame, cv::Ptr <cv::aruco::Dictionary> dictionary) 
{
        std::vector<int> ids;
        std::vector<std::vector<cv::Point2f> > corners;
        cv::Ptr <cv::aruco::DetectorParameters> param = cv::aruco::DetectorParameters::create();
        param->markerBorderBits = 2;
        cv::aruco::detectMarkers(frame, dictionary, corners, ids, param);
    
        // if at least one marker detected
        if (ids.size() > 0)
        {
            cv::aruco::drawDetectedMarkers(frame, corners, ids);
        }
    return frame;
}



int main(int, char**) {

    bool detect = true;
    int markerSize = 5;
    cv::Mat marker, white; 
    int id=1, pixels = 400, borderBits = 2, left = 10, right = 10, top = 10, bottom = 10;

    cv::Ptr <cv::aruco::Dictionary> dictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_6X6_250);
    cv::Ptr <cv::aruco::Dictionary> customDictionary = cv::aruco::generateCustomDictionary (1,markerSize,0);

    customDictionary->markerSize = markerSize;
    customDictionary->maxCorrectionBits = 3;
    bool data[markerSize][markerSize] =  {
        {1,1,0,1,1},
        {1,1,0,1,1},
        {1,0,1,0,1},
        {0,1,1,0,0},
        {1,0,1,1,1}
        };


    cv::Mat markerBits (markerSize,markerSize,CV_8UC1, &data);
    cv::Mat markerCompressed = cv::aruco::Dictionary::getByteListFromBits(markerBits);
    customDictionary->bytesList.push_back(markerCompressed);


    white = cv::Mat::ones(pixels + top + bottom, pixels + right + left,CV_8UC1)*255;
    cv::aruco::drawMarker(customDictionary,id,pixels,marker,borderBits);
    cv::copyMakeBorder(marker, white, top, bottom, left, right, cv::BORDER_CONSTANT, 255);

    cv::imshow("Custom marker", white);


    cv::VideoCapture inputVideo;
    inputVideo.open(0);

    while (inputVideo.grab()) {
        cv::Mat image;
        inputVideo.retrieve(image);
     
        cv::Mat test;
            
        if(detect)
        {
            cv::imshow("out", detectMarkers(image, customDictionary));
        }
        char key = (char) cv::waitKey(10);
        if (key == 27)
            break;
    }

    return 0;
}